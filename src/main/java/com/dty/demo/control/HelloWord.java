package com.dty.demo.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author dty
 * @version 1.0
 * @date 2021/4/7 17:18
 * @description
 */
@Controller
public class HelloWord {
    @RequestMapping("/index")
    public String sayHello(){
        return "index";
    }
}
