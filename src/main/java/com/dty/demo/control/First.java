package com.dty.demo.control;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Writer;

/**
 * @author dty
 * @version 1.0
 * @date 2021/4/8 9:39
 * @description
 */
@RestController
@RequestMapping("/employee")
public class First {
    private static Logger logger=LoggerFactory.getLogger(First.class);
    @RequestMapping("/First")
    private Object getfirst(HttpServletRequest request, HttpServletResponse response){
        System.out.println(request.getServerName());

        return response;

    }

    @GetMapping("/list")
    private Object get(){
        logger.info("employee.list is used");
        System.out.println("5555555555555555555");
        JSONObject object=new JSONObject();
        object.put("code","1");
        object.put("msg","22");
        return object;
    }
}
