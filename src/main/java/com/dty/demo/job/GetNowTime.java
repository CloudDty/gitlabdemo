package com.dty.demo.job;

import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author dty
 * @version 1.0
 * @date 2021/4/25 16:22
 * @description
 */
public class GetNowTime {

    @Scheduled(cron = "0 26 16 * * ?")
    private void getNow(){
        System.out.println("定时任务启动");
    }

    @Scheduled(fixedDelay = 5000)//执行间隔
    private void getNow1(){

    }

}
