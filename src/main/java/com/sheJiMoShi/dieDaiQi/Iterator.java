package com.sheJiMoShi.dieDaiQi;

/**
 * 2 * @Author:丁同运
 * 3 * @Date: 2020/11/18
 * 4
 */
public interface Iterator {
    public abstract Object next();

    Boolean hasNext();
}
