package com.sheJiMoShi.dieDaiQi;

import java.util.ArrayList;
import java.util.List;

/**
 * 2 * @Author:丁同运
 * 3 * @Date: 2020/11/18
 * 4
 */
public class BookShelf implements Aggregate {

    private List<Book> list = new ArrayList<>();

    @Override
    public Iterator interator() {
        return new BookShelfIterator(this);
    }

    public int getLenth() {
        return list.size();
    }

    public boolean add(Book entity) {
        return list.add(entity);
    }

    public Book getBook(int index) {
        return list.get(index);

    }

    public Book remove(int index) {
        return list.remove(index);
    }
}
