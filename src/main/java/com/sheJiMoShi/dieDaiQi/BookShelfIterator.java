package com.sheJiMoShi.dieDaiQi;

/**
 * 2 * @Author:丁同运
 * 3 * @Date: 2020/11/18
 * 4
 */
public class BookShelfIterator implements Iterator {
    private BookShelf bookShelf;
    private int index = 0;

    public BookShelfIterator(BookShelf bookShelf) {
        this.bookShelf = bookShelf;
    }

    @Override
    public Object next() {
        Book book = bookShelf.getBook(index);
        index++;
        return book;
    }

    @Override
    public Boolean hasNext() {
        return index < bookShelf.getLenth();
    }
}
