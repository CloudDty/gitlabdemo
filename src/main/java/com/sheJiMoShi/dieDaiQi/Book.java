package com.sheJiMoShi.dieDaiQi;

import lombok.Getter;
import lombok.Setter;

/**
 * 2 * @Author:丁同运
 * 3 * @Date: 2020/11/18
 * 4
 */
@Getter
@Setter
public class Book {
    private String name;

    public Book(String name) {
        this.name = name;
    }

}
