package com.util;

/**
 * 2 * @Author:丁同运
 * 3 * @Date: 2020/11/18
 * 4根据两个点的方向角判断是否转弯
 */
public class FxjUtil {
    private static final double limit = 45.0;//转弯的最小角度

    /**
     * @param oldAngle sha
     * @param newAngle
     * @return
     */
    public static boolean isZW(double oldAngle, double newAngle) {
        double right = oldAngle + limit >= 360 ? oldAngle + limit - 360 : oldAngle + limit;
        double left = oldAngle - limit >= 0 ? oldAngle - limit : oldAngle - limit + 360;
        if (right > left) {
            return !(newAngle < right && newAngle > left);
        } else {
            return newAngle >= right && newAngle <= left;
        }
    }

    public static void main(String[] args) {
        double a = 10.0;
        while (a < 360) {
            double b = a + 45 >= 360 ? a + 45 - 360 : a + 45;
            double b1 = a + 40 >= 360 ? a + 40 - 360 : a + 40;
            double c = a - 45 >= 0 ? a - 45 : a - 45 + 360;
            double c1 = a - 40 >= 0 ? a - 40 : a - 40 + 360;
            System.out.println("a=" + a + ";b=" + b + ";" + FxjUtil.isZW(a, b));
            System.out.println("a=" + a + ";b1=" + b1 + ";" + FxjUtil.isZW(a, b1));
            System.out.println("a=" + a + ";c=" + c + ";" + FxjUtil.isZW(a, c));
            System.out.println("a=" + a + ";c1=" + c1 + ";" + FxjUtil.isZW(a, c1));

            a += 45.0;
        }
    }


}
