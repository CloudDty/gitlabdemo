package com.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.SimpleFormatter;

/**
 * @author dty
 * @version 1.0
 * @date 2021/4/25 16:29
 * @description
 */
public class DateTimeUtil {
    public final String parden0="yyyy-MM-dd HH:mm:ss";

    private String format19(Date date){
    return new SimpleDateFormat(parden0).format(date);
    }
}
